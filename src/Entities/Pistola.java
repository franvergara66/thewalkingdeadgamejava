package Entities;

import Main.SimulationManager;
import Main.Village;

public class Pistola {
	private int speed;
	private Bala bala;
	private int idB = 0;

	public Pistola(int speed) {
		this.speed = speed;
	}

	public void dispararBala(Coords posHumano, int direccion, Village vill) {
		bala = new Bala(idB, speed, direccion, vill);
		bala.setCoords(posHumano.getI(), posHumano.getJ());
		idB++;
		SimulationManager.addBala(bala);
		SimulationManager.getIndicadores().setInd(0, 0, -1);
		bala.start();
	}
}
