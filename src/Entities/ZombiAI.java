package Entities;

import java.awt.Rectangle;

public class ZombiAI {
	Rectangle visionArea = new Rectangle();
	
	public int randomDirection() {

		return 37 + (int) (Math.random() * 4);
	}

	public int toHumanDirection(Coords posZ, Coords posH) {
		
		int d1, d2, d3, d4, x1, x2, x3, y1, y2, y3;

		x1 = (posZ.getI() + 1) - posH.getI();
		x2 = (posZ.getI() - 1) - posH.getI();
		x3 = posZ.getI() - posH.getI();
		y1 = (posZ.getJ() + 1) - posH.getJ();
		y2 = (posZ.getJ() - 1) - posH.getJ();
		y3 = posZ.getJ() - posH.getJ();

		d1 = x1 * x1 + y3 * y3;
		d2 = x2 * x2 + y3 * y3;
		d3 = x3 * x3 + y2 * y2;
		d4 = x3 * x3 + y1 * y1;

		int menor = d1;
		int dir = 40;
		if (d2 < menor) {
			dir = 38;
			menor = d2;
		}
		if (d3 < menor) {
			dir = 37;
			menor = d3;
		}
		if (d4 < menor) {
			dir = 39;
			menor = d4;
		}

		return dir;
	}
	
	public void updateVisionArea(Coords posZ, Coords posH) {
		visionArea.setBounds(posZ.getI()-2,posZ.getJ()-2,5,5);
	}
	
	public int nextStep(Coords posZ, Coords posH) {
		updateVisionArea(posZ, posH);
		
		if(visionArea.contains(posH.getI(), posH.getJ())) {
			return toHumanDirection(posZ, posH);
		}
		else 
			return randomDirection();
	}

}
