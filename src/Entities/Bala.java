package Entities;

import Main.Village;

public class Bala extends Thread {
	private Coords coords;
	private Village vill;
	private int id;
	private int speed;
	private int direccion;
	private boolean keepRunning = true;

	public Bala(int idB) {
		this.id = idB;
	}

	public Bala(int id, int speed, int direccion, Village vill) {
		this.vill = vill;
		coords = new Coords(0, 0, 'H');
		this.id = id;
		this.speed = speed;
		this.direccion = direccion;
	}

	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;
		if (other == this)
			return true;
		if (this.id == ((Bala) other).id)
			return true;
		if (!(other instanceof Bala))
			return false;
		return false;
	}

	public Coords getCoords() {
		return coords;
	}

	public char getHold() {
		return coords.getHold();
	}

	@Override
	public void run() {
		while (keepRunning) {
			vill.moverBala(id, direccion, coords);
			try {
				Thread.sleep(speed);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void setCoords(int i, int j) {
		coords.setCords(i, j);
	}

	public void setHold(char hold) {
		coords.setHold(hold);
	}

	public void stopRunning() {
		this.keepRunning = false;
	}
}
