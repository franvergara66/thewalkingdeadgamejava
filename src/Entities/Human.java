package Entities;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import Main.Village;

public class Human extends Thread {
	private Village vill;
	private Pistola pistola;
	private Coords coords;
	private int direccion = 0;

	public Human(int coldWeaponTime, int bulletSpeed, Village village) {
		this.vill = village;
		this.coords = new Coords(0, 0, '.');
		this.pistola = new Pistola(bulletSpeed);
	}

	public Coords getCoords() {
		return this.coords;
	}

	@Override
	public void run() {
		vill.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				int code = e.getKeyCode();
				if (code != KeyEvent.VK_SPACE) {
					vill.moverHumano(e.getKeyCode());
					direccion = code;
				} else if (direccion != 0) {
					pistola.dispararBala(coords, direccion, vill);
				}
			}
		});
	}

	public void setCoords(int i, int j) {
		coords.setCords(i, j);
	}

	public int getDireccion() {
		return this.direccion;
	}
}
