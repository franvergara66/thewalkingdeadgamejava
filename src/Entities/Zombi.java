package Entities;

import Main.SimulationManager;
import Main.Village;

public class Zombi extends Thread {
	private int speed;
	private Village vill;
	private int id;
	private int coldDown;
	private Coords coords;
	private int direccion = 0;
	private ZombiAI AI;
	private boolean iHaveToSleep = false;
	private boolean keepRunning = true;

	public Zombi(int id, int speed, Village village) {
		this.vill = village;
		this.coords = new Coords(0, 0, '.');
		this.id = id;
		this.speed = speed;
		AI = new ZombiAI();
	}

	public int getIdZ() {
		return this.id;
	}

	public Coords getCoords() {
		return this.coords;
	}

	public char getHold() {
		return coords.getHold();
	}

	@Override
	public void run() {
		try {
			while (keepRunning) {
				if (iHaveToSleep) {
					Thread.sleep(coldDown);
					iHaveToSleep = false;
				}
				direccion = AI.nextStep(coords,
						SimulationManager.humanCoords());
				vill.moverZombi(id, direccion);
				Thread.sleep(speed);
			}
		} catch (InterruptedException e) {
			this.interrupt();
		}
	}

	public void setCoords(int i, int j) {
		coords.setCords(i, j);
	}

	public void setHold(char hold) {
		coords.setHold(hold);
	}

	public void stopRunning() {
		this.keepRunning = false;
	}

	public void youHaveToSleep(int millis) {
		coldDown = millis;
		iHaveToSleep = true;
	}

	public int getDireccion() {
		return this.direccion;
	}

	public boolean isFrozen() {
		return iHaveToSleep;
	}
}
