package Main;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Spliter {
	public static Image[][] Split(int tam, String imageName) throws IOException {
		File file = new File(imageName);
		FileInputStream fis = new FileInputStream(file);
		BufferedImage image = ImageIO.read(fis);

		int rows = image.getHeight() / tam;
		int cols = image.getWidth() / tam;

		Image result[][] = new Image[rows][cols];

		BufferedImage imgs[][] = new BufferedImage[rows][cols];
		for (int x = 0; x < rows; x++) {
			for (int y = 0; y < cols; y++) {
				imgs[x][y] = new BufferedImage(tam, tam, image.getType());
				Graphics2D gr = imgs[x][y].createGraphics();
				gr.drawImage(image, 0, 0, tam, tam, tam * y, tam * x, tam * y
						+ tam, tam * x + tam, null);
				gr.dispose();
				result[x][y] = Toolkit.getDefaultToolkit().createImage(
						image.getSource());
			}
		}
		return imgs;
	}
}
