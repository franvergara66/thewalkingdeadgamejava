package Main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import Entities.Coords;

import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

public class OutputScreen {

	private JFrame frmZombies;
	private JTextField municionText;
	private JTextField puntuacionText;
	private MyCanvas canvas;
	private JProgressBar progressBar;
	private JPanel panel;
	private JLabel lblPuntuaciones;
	private JButton btnAgregarPuntuacion;
	private DefaultTableModel tb;
	private JTable table;
	private JScrollPane scrollPane_1;
	private JPanel panel_2;
	private JPanel panel_5;
	private JPanel panel_1;
	private JPanel panel_4;

	/**
	 * Create the application.
	 * 
	 * @throws IOException
	 */
	public OutputScreen() throws IOException {
		initialize();
	}

	public JFrame getFrmZombies() {
		return frmZombies;
	}

	public JTextField getMunicionText() {
		return municionText;
	}

	public JProgressBar getProgressBar() {
		return progressBar;
	}

	public JTextField getPuntuacionText() {
		return puntuacionText;
	}

	private JTable getTable() {
		return table;
	}

	public JTextField getTextField() {
		return getMunicionText();
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws IOException
	 */
	@SuppressWarnings("serial")
	private void initialize() throws IOException {
		setFrmZombies(new JFrame());
		getFrmZombies().setTitle("Zombies 101");
		getFrmZombies().setBounds(235, 10,
				235 + SimulationManager.getLimit() * 32,
				80 + SimulationManager.getLimit() * 32);
		getFrmZombies().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 534, 0 };
		gridBagLayout.rowHeights = new int[] { 375, 45, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		frmZombies.getContentPane().setLayout(gridBagLayout);

		panel_1 = new JPanel();
		panel_1.setFocusable(false);
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 0;
		frmZombies.getContentPane().add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 320, 222, 0 };
		gbl_panel_1.rowHeights = new int[] { 361, 0 };
		gbl_panel_1.columnWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		panel_4 = new JPanel();
		panel_4.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		panel_4.setAlignmentX(Component.RIGHT_ALIGNMENT);
		panel_4.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_4.setFocusable(false);
		panel_4.setMinimumSize(new Dimension(SimulationManager.getLimit() * 32,
				SimulationManager.getLimit() * 32));
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.fill = GridBagConstraints.BOTH;
		gbc_panel_4.insets = new Insets(0, 0, 0, 5);
		gbc_panel_4.gridx = 0;
		gbc_panel_4.gridy = 0;
		panel_1.add(panel_4, gbc_panel_4);
		GridBagLayout gbl_panel_4 = new GridBagLayout();
		gbl_panel_4.columnWidths = new int[] { 320, 0 };
		gbl_panel_4.rowHeights = new int[] { 320, 0 };
		gbl_panel_4.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_4.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel_4.setLayout(gbl_panel_4);

		setCanvasArea(new MyCanvas());
		canvas.setFont(new Font("Consolas", Font.PLAIN, 13));

		JPanel panel_3 = new JPanel();
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.anchor = GridBagConstraints.EAST;
		gbc_panel_3.fill = GridBagConstraints.VERTICAL;
		gbc_panel_3.gridx = 1;
		gbc_panel_3.gridy = 0;
		panel_1.add(panel_3, gbc_panel_3);
		panel_3.setFocusable(false);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[] { 202, 0 };
		gbl_panel_3.rowHeights = new int[] { 0, 320, 0 };
		gbl_panel_3.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_3.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		panel_3.setLayout(gbl_panel_3);

		panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.anchor = GridBagConstraints.EAST;
		gbc_panel_2.insets = new Insets(0, 0, 5, 0);
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 0;
		panel_3.add(panel_2, gbc_panel_2);

		lblPuntuaciones = new JLabel("Puntuaciones:");
		panel_2.add(lblPuntuaciones);
		lblPuntuaciones.setFocusable(false);

		btnAgregarPuntuacion = new JButton("Agregar Puntaje");
		panel_2.add(btnAgregarPuntuacion);
		btnAgregarPuntuacion.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String name = (String) JOptionPane.showInputDialog(frmZombies,
						"Nombre: ", "Introduce tu nombre",
						JOptionPane.PLAIN_MESSAGE, null, null, null);
				if (name != null) {
					tb.addRow(new Object[] { name,
							getPuntuacionText().getText() });
				}
			}
		});
		btnAgregarPuntuacion.setFocusable(false);

		panel_5 = new JPanel();
		GridBagConstraints gbc_panel_5 = new GridBagConstraints();
		gbc_panel_5.anchor = GridBagConstraints.EAST;
		gbc_panel_5.fill = GridBagConstraints.VERTICAL;
		gbc_panel_5.gridx = 0;
		gbc_panel_5.gridy = 1;
		panel_3.add(panel_5, gbc_panel_5);
		GridBagLayout gbl_panel_5 = new GridBagLayout();
		gbl_panel_5.columnWidths = new int[] { 222, 0 };
		gbl_panel_5.rowHeights = new int[] { 320, 0 };
		gbl_panel_5.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_5.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel_5.setLayout(gbl_panel_5);

		scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridx = 0;
		gbc_scrollPane_1.gridy = 0;
		panel_5.add(scrollPane_1, gbc_scrollPane_1);
		scrollPane_1.setFocusable(false);
		scrollPane_1.setPreferredSize(new Dimension(100, 100));

		setTable(new JTable(tb));
		scrollPane_1.setViewportView(getTable());
		getTable().setModel(
				new DefaultTableModel(new Object[][] {}, new String[] {
						"Nombre", "Puntaje" }) {
					boolean[] columnEditables = new boolean[] { false, false };

					@Override
					public boolean isCellEditable(int row, int column) {
						return columnEditables[column];
					}
				});
		tb = (DefaultTableModel) table.getModel();
		RowSorter<DefaultTableModel> sorter = new TableRowSorter<DefaultTableModel>(
				tb);
		table.setRowSorter(sorter);
		{
			panel = new JPanel();
			panel.setFocusable(false);
			panel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207,
					229)), "Indicadores", TitledBorder.LEADING,
					TitledBorder.TOP, null, null));
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.fill = GridBagConstraints.BOTH;
			gbc_panel.gridx = 0;
			gbc_panel.gridy = 1;
			frmZombies.getContentPane().add(panel, gbc_panel);
			panel.setLayout(null);

			setProgressBar(new JProgressBar());
			getProgressBar().setValue(100);
			getProgressBar().setStringPainted(true);

			JLabel lblVida = new JLabel("Vida:");
			lblVida.setFocusable(false);
			lblVida.setBounds(5, 24, 29, 14);
			panel.add(lblVida);

			JLabel lblMunicion = new JLabel("Municion:");
			lblMunicion.setFocusable(false);
			lblMunicion.setBounds(220, 24, 55, 14);
			panel.add(lblMunicion);

			setTextField(new JTextField());
			getTextField().setEditable(false);
			getTextField().setColumns(10);

			JLabel lblPuntuacion = new JLabel("Puntuacion:");
			lblPuntuacion.setFocusable(false);
			lblPuntuacion.setBounds(360, 24, 80, 14);
			panel.add(lblPuntuacion);

			setPuntuacionText(new JTextField());
			getPuntuacionText().setEditable(false);
			getPuntuacionText().setColumns(10);
		}
	}

	/**
	 * Launch the application.
	 */
	public void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					OutputScreen window = new OutputScreen();
					window.getFrmZombies().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void setFrmZombies(JFrame frmZombies) {
		this.frmZombies = frmZombies;
		frmZombies.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				SimulationManager.finish();
				System.out.print("FINALIZANDO SIMULADOR..");
			}
		});
		frmZombies.setResizable(false);
		frmZombies.getContentPane().setFocusable(false);
		frmZombies.getContentPane().setFocusTraversalKeysEnabled(false);
		frmZombies.setMinimumSize(new Dimension(556, 410));
	}

	public void setMunicionText(JTextField municionText) {
		this.municionText = municionText;
	}

	public void setProgressBar(JProgressBar progressBar) {
		this.progressBar = progressBar;
		progressBar.setFocusable(false);
		progressBar.setBounds(44, 24, 158, 14);
		panel.add(progressBar);
	}

	public void setPuntuacionText(JTextField puntuacionText) {
		this.puntuacionText = puntuacionText;
		puntuacionText.setText("0");
		puntuacionText.setFocusable(false);
		puntuacionText.setBounds(458, 18, 70, 20);
		panel.add(puntuacionText);
	}

	private void setTable(JTable table) {
		this.table = table;
		table.setFocusable(false);
	}

	public void setCanvasArea(MyCanvas canvas1) {
		this.canvas = canvas1;
		GridBagConstraints gbc_canvas = new GridBagConstraints();
		gbc_canvas.fill = GridBagConstraints.BOTH;
		gbc_canvas.gridx = 0;
		gbc_canvas.gridy = 0;
		panel_4.add(canvas, gbc_canvas);
		canvas.setVisible(false);
		canvas.setFocusable(false);
	}

	public void setTextField(JTextField textField) {
		this.setMunicionText(textField);
		getMunicionText().setText("0");
		getMunicionText().setFocusable(false);
		getMunicionText().setBounds(293, 18, 49, 20);
		panel.add(getMunicionText());
	}

	public void updateScreen(Coords ini, Coords end) {
		if (ini == null && end == null) {
			canvas.setVisible(true);
			canvas.repaint();
		} else {
			if (end.getJ() > ini.getJ()) {
				canvas.repaint(ini.getJ() * 32, ini.getI() * 32, 64, 32);
			} else if (end.getI() > ini.getI()) {
				canvas.repaint(ini.getJ() * 32, ini.getI() * 32, 32, 64);
			} else if (end.getJ() < ini.getJ()) {
				canvas.repaint(end.getJ() * 32, end.getI() * 32, 64, 32);
			} else if (end.getI() < ini.getI()) {
				canvas.repaint(end.getJ() * 32, end.getI() * 32, 32, 64);
			} else if (end.equals(ini)) {
				canvas.repaint(ini.getJ() * 32, end.getI() * 32, 32, 32);
			}
		}
	}
}
