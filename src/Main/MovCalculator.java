package Main;

import java.awt.event.KeyEvent;

import Entities.Bala;
import Entities.Human;
import Entities.Zombi;

public class MovCalculator {

	public void moverBala(int Direccion, int limit, Bala bala) {
		if (Direccion == KeyEvent.VK_LEFT) {
			bala.getCoords().movLeft();
		}
		if (Direccion == KeyEvent.VK_UP) {
			bala.getCoords().movUp();
		}
		if (Direccion == KeyEvent.VK_RIGHT) {
			bala.getCoords().movRight(limit);
		}
		if (Direccion == KeyEvent.VK_DOWN) {
			bala.getCoords().movDown(limit);
		}
	}

	public void moverHumano(int Direccion, int limit, Human human) {
		if (Direccion == KeyEvent.VK_LEFT) {
			human.getCoords().movLeft();
		}
		if (Direccion == KeyEvent.VK_UP) {
			human.getCoords().movUp();
		}
		if (Direccion == KeyEvent.VK_RIGHT) {
			human.getCoords().movRight(limit);
		}
		if (Direccion == KeyEvent.VK_DOWN) {
			human.getCoords().movDown(limit);
		}
	}

	public void moverZombi(int Direccion, int limit, Zombi zombi) {
		if (Direccion == KeyEvent.VK_LEFT) {
			zombi.getCoords().movLeft();
		}
		if (Direccion == KeyEvent.VK_UP) {
			zombi.getCoords().movUp();
		}
		if (Direccion == KeyEvent.VK_RIGHT) {
			zombi.getCoords().movRight(limit);
		}
		if (Direccion == KeyEvent.VK_DOWN) {
			zombi.getCoords().movDown(limit);
		}
	}

}
