package Main;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MyCanvas extends JPanel {
	private int pH = 0;
	private int pB = 0;
	private Image zombi[][];
	private Image human[][];
	private Image zombi_congelado[][];
	private Image objetos[][];
	private Image municion[][];
	private Image grama;
	private Image background;
	public MyCanvas() throws IOException {
		super();

		zombi = Spliter.Split(32, "C:/ProyectoJava2012/src/zombi.png");
		zombi_congelado = Spliter.Split(32, "C:/ProyectoJava2012/src/zombi_congelado.png");
		human = Spliter.Split(32, "C:/ProyectoJava2012/src/human.png");
		objetos = Spliter.Split(32, "C:/ProyectoJava2012/src/objetos.png");
		municion = Spliter.Split(32, "C:/ProyectoJava2012/src/municion.png");
		grama = Toolkit.getDefaultToolkit().getImage("C:/ProyectoJava2012/src/grama.png");

		BufferedImage bg = null;
		bg = ImageIO.read(new FileInputStream(new File("C:/ProyectoJava2012/src/grama.png")));
		BufferedImage bufferedImage = new BufferedImage(
				SimulationManager.getLimit() * 32,
				SimulationManager.getLimit() * 32, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bufferedImage.createGraphics();
		for (int i = 0; i < SimulationManager.getLimit(); i++) {
			for (int j = 0; j < SimulationManager.getLimit(); j++) {
				g.drawImage(bg, i * 32, j * 32, null);
			}
		}
		background = Toolkit.getDefaultToolkit().createImage(
				bufferedImage.getSource());
	}

	private int direccionSprite(int direccion) {
		if (direccion == KeyEvent.VK_DOWN)
			return 0;
		if (direccion == KeyEvent.VK_LEFT)
			return 1;
		if (direccion == KeyEvent.VK_RIGHT)
			return 2;
		if (direccion == KeyEvent.VK_UP)
			return 3;
		return 0;
	}

	public void drawBoard(final Graphics g) {
		new Runnable() {
			@Override
			public void run() {
				Rectangle s = g.getClipBounds();
				g.drawImage(background, 0, 0, null);
				for (int i = s.y / 32; i < (s.y + s.height) / 32; i++) {
					for (int j = s.x / 32; j < (s.x + s.width) / 32; j++) {
						Image di = grama;
						char item = SimulationManager.getVillage().itemAt(i, j);

						if (item == 'H') {
							int direccion = SimulationManager.humanDireccion();
							di = human[direccionSprite(direccion)][pH];

						} else if (item == '*')
							di = objetos[0][1];
						else if (item == 'C')
							di = objetos[1][1];
						else if (item == 'M')
							di = objetos[1][0];
						else if (item == 'Z') {
							int idZ = SimulationManager.findZombieID(i, j);
							if (idZ != -1) {
								int direccion = SimulationManager
										.zombiDireccion(idZ);
								if (!SimulationManager.isZombiFrozen(idZ))
									di = zombi[direccionSprite(direccion)][pH];
								else
									di = zombi_congelado[direccionSprite(direccion)][0];
							}
						} else if (item == '�') {
							if (pB == 0)
								di = objetos[2][0];
							if (pB == 1)
								di = objetos[2][1];
							if (pB == 2)
								di = objetos[3][0];
						} else if (item > 47 && item < 58) {
							di = municion[0][item - 49];
						}
						g.drawImage(di, j * 32, i * 32, null);
					}
				}
				pH = (pH + 1) % 3;
				pB = (pB + 1) % 3;
			}
		}.run();
	}

	@Override
	public void paintComponent(Graphics g) {
		drawBoard(g);
	}
}
