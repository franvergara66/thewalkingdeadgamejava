package Main;

import java.util.Vector;

import Entities.Bala;
import Entities.Coords;
import Entities.Human;
import Entities.Zombi;

public class SimulationManager {
	private static Vector<Zombi> zombies;
	private static Human human;
	private static Vector<Bala> balas;
	private static Village village;
	private static MovCalculator mov;
	private static Indicadores ind;
	private static int coldWeaponTime;
	private static int speedZombies;
	private static int numZombies;
	private static int bulletSpeed;
	private static int limit;
	private static boolean started = false;
	public static String[] evalDir = { "Oeste", "Norte", "Este", "Sur" };
	private static boolean lost;

	public static void addBala(Bala bala) {
		SimulationManager.balas.add(bala);
	}

	public static void dormirZombiThread(int idZ, int millis) {
		zombies.get(idZ).youHaveToSleep(millis);
	}

	public static void dormirZombiThread(int x, int y, int millis) {
		for (int i = 0; i < getNumZombies(); i++) {
			if (zombies.get(i).getCoords().getI() == x
					&& zombies.get(i).getCoords().getJ() == y) {
				zombies.get(i).youHaveToSleep(
						SimulationManager.getColdWeaponTime());
				return;
			}
		}
	}

	public static int findZombieID(int x, int y) {
		for (int i = 0; i < getNumZombies(); i++) {
			if (zombies.get(i).getCoords().getI() == x
					&& zombies.get(i).getCoords().getJ() == y)
				return i;
		}
		return -1;
	}

	public static void finish() {
		SimulationManager.terminateZombiesThreads();
		SimulationManager.interruptHumanThread();
	}

	public static int getBulletSpeed() {
		return bulletSpeed;
	}

	public static int getColdWeaponTime() {
		return coldWeaponTime;
	}

	public static Indicadores getIndicadores() {
		return ind;
	}

	public static int getNumZombies() {
		return numZombies;
	}

	public static OutputScreen getOut() {
		return village.getOut();
	}

	public static int getSpeedZombies() {
		return speedZombies;
	}

	public static Village getVillage() {
		return village;
	}

	public static void humanSetCord(int i, int j) {
		human.setCoords(i, j);
	}

	public static void interruptHumanThread() {
		human.interrupt();
	}

	public static void interruptZombieThread(int idZ) {
		zombies.get(idZ).interrupt();
	}

	public static void interruptZombieThread(int x, int y) {
		for (int i = 0; i < getNumZombies(); i++) {
			if (zombies.get(i).getCoords().getI() == x
					&& zombies.get(i).getCoords().getJ() == y) {
				zombies.get(i).interrupt();
				return;
			}
		}
	}

	public static Coords moverBala(int idB, int Direccion, int limit) {
		int posB = balas.indexOf(new Bala(idB));
		mov.moverBala(Direccion, limit, balas.get(posB));
		return balas.get(posB).getCoords();
	}

	public static Coords moverHumano(int Direccion, int limit) {
		mov.moverHumano(Direccion, limit, human);
		return human.getCoords();
	}

	public static Coords moverZombi(int idZ, int Direccion, int limit) {
		mov.moverZombi(Direccion, limit, zombies.get(idZ));
		return zombies.get(idZ).getCoords();
	}

	public static void start() {
		if (!started) {
			village.setDataValues();
			village.printPueblo(null, null);
			human.start();
			for (int i = 0; i < getNumZombies(); i++) {
				zombies.get(i).start();
				started = true;
			}
		}
	}

	public static void terminarBalaThread(int idB) {
		int posB = balas.indexOf(new Bala(idB));
		balas.get(posB).stopRunning();
		balas.remove(posB);
	}

	public static void terminateZombiesThreads() {
		for (int i = 0; i < getNumZombies(); i++) {
			if (zombies.get(i).isAlive())
				zombies.get(i).stopRunning();
		}
	}

	public static void zombiSetCord(int idZ, int i, int j) {
		zombies.get(idZ).setCoords(i, j);
	}

	public static int getLimit() {
		return limit;
	}

	public static boolean isHumanAt(Coords x) {
		return human.getCoords() == x;
	}

	public static int humanDireccion() {
		return human.getDireccion();
	}

	public static int zombiDireccion(int idZ) {
		return zombies.get(idZ).getDireccion();
	}

	public static boolean isZombiFrozen(int idZ) {
		return zombies.get(idZ).isFrozen();
	}

	public SimulationManager(char[][] mVill, int X, int numZombies,
			int speedZombies, int coldWeaponTime, int bulletSpeed) {
		SimulationManager.mov = new MovCalculator();
		SimulationManager.coldWeaponTime = coldWeaponTime;
		SimulationManager.speedZombies = speedZombies;
		SimulationManager.bulletSpeed = bulletSpeed;
		SimulationManager.numZombies = numZombies;
		SimulationManager.limit = X;
		SimulationManager.zombies = new Vector<Zombi>();
		SimulationManager.village = new Village(mVill, X, numZombies);
		SimulationManager.human = new Human(coldWeaponTime, bulletSpeed,
				village);
		SimulationManager.ind = new Indicadores();
		SimulationManager.lost = false;

		for (int i = 0; i < numZombies; i++) {
			SimulationManager.zombies.add(new Zombi(i, speedZombies, village));
		}
		SimulationManager.balas = new Vector<Bala>();
		start();
	}

	public static Coords humanCoords() {
		return human.getCoords();
	}

	public static void lost() {
		if (!lost) {

			zombies.add(new Zombi(numZombies, speedZombies, village));
			zombies.get(numZombies).setCoords(human.getCoords().getI(),
					human.getCoords().getJ());
			human.setCoords(-10, -10);
			zombies.get(numZombies).start();
			numZombies++;
		}
		lost = true;
	}
}
