package Main;

import java.awt.event.KeyAdapter;
import java.util.Vector;

import Entities.Coords;

public class Village {
	private VillagePos mVillPos[][];
	private char mVill[][];
	private int X;
	private Coords human;
	private Coords zombi[];
	private Vector<Coords> balas;
	private int nItems = 0;
	private boolean gano = false;
	private boolean perdio = false;
	private OutputScreen out;

	public Village(char[][] mVill, int X, int nZombies) {
		this.human = new Coords(0, 0, '.');
		this.balas = new Vector<Coords>();
		this.setmVill(new char[X][X]);
		this.setmVill(mVill);
		this.zombi = new Coords[nZombies + 1];
		this.X = X;
		this.setmVillPos();
		this.setDisplay();
	}

	public void addKeyListener(KeyAdapter ka) {
		getOut().getFrmZombies().addKeyListener(ka);
	}

	public OutputScreen getOut() {
		return out;
	}

	int getX() {
		return X;
	}

	public void moverBala(int idB, int Direccion, Coords initC) {
		if (!gano && !perdio) {
			Coords bala;
			Coords ini;
			boolean coldAttack = false;
			int pos;
			if ((pos = balas.indexOf(initC)) == -1) {
				balas.add(initC);
				bala = initC;
				pos = balas.size() - 1;
			} else {
				bala = balas.get(pos);
			}
			ini = new Coords(bala.getI(), bala.getJ(), '.');
			if (bala.getHold() != 'H')
				mVillPos[bala.getI()][bala.getJ()].set(bala.getHold());

			bala = SimulationManager.moverBala(idB, Direccion, X);

			if (SimulationManager.getIndicadores().getTotalMun() == -1)
				coldAttack = true;

			char nextItem = mVillPos[bala.getI()][bala.getJ()]
					.getAttack(coldAttack);
			if (nextItem == '*') {
				SimulationManager.terminarBalaThread(idB);
				balas.remove(pos);
			} else if (nextItem == 'Z') {
				int idZ = SimulationManager.findZombieID(bala.getI(),
						bala.getJ());
				if (!coldAttack) {
					mVillPos[bala.getI()][bala.getJ()]
							.set(zombi[idZ].getHold());
					SimulationManager.getIndicadores().setInd(0, 500, 0);
				}
				SimulationManager.terminarBalaThread(idB);
				balas.remove(pos);
			} else if (nextItem == '�') {
				bala.setHold('.');
				mVillPos[bala.getI()][bala.getJ()].set('.');
			} else if (ini.equals(bala)) {
				mVillPos[bala.getI()][bala.getJ()].set(bala.getHold());
				SimulationManager.terminarBalaThread(idB);
				balas.remove(pos);
			} else {
				bala.setHold(nextItem);
				mVillPos[bala.getI()][bala.getJ()].set('�');
			}
			printPueblo(ini, bala);
		}
	}

	public void moverHumano(int Direccion) {
		if (!gano && !perdio) {

			Coords ini = new Coords(human.getI(), human.getJ(), '.');
			mVillPos[ini.getI()][ini.getJ()].set('.');
			human = SimulationManager.moverHumano(Direccion, X);

			char nextItem = mVillPos[human.getI()][human.getJ()].get();
			if (nextItem == '*') {
				SimulationManager.getIndicadores().setInd(-5, -10, 0);
				human.setCords(ini.getI(), ini.getJ());
			} else if (nextItem > 47 && nextItem < 58) {
				int mn = nextItem - 48;
				SimulationManager.getIndicadores().setInd(0, mn * 50, mn);
				nItems--;
			} else if (nextItem == 'M') {
				SimulationManager.getIndicadores().setInd(20, 100, 0);
				nItems--;
			} else if (nextItem == 'C') {
				SimulationManager.getIndicadores().setInd(0, 150, 0);
				nItems--;
			} else if (nextItem == 'Z') {
				SimulationManager.getIndicadores().setInd(-15, -50, 0);
				human.setCords(ini.getI(), ini.getJ());
			}

			mVillPos[human.getI()][human.getJ()].set('H');
			SimulationManager.humanSetCord(human.getI(), human.getJ());
			printPueblo(ini, human);

			if (nItems == 0) {
				out.getFrmZombies().setTitle("Zombies 101 - �RECOGISTE TODOS LOS ITEMS!");
				gano = true;
				SimulationManager.finish();
			}
			if (getOut().getProgressBar().getValue() == 0) {
				out.getFrmZombies().setTitle("Zombies 101 - �TE HAS CONVERTIDO EN UN ZOMBI!");
				perdio = true;
				mVillPos[human.getI()][human.getJ()].set('Z');
				zombi[zombi.length - 1] = new Coords(human);
				SimulationManager.lost();
				SimulationManager.interruptHumanThread();
			}
		}
	}

	public void moverZombi(int id, int Direccion) {
		if (!gano) {
			Coords ini = new Coords(zombi[id].getI(), zombi[id].getJ(),
					zombi[id].getHold());

			zombi[id] = SimulationManager.moverZombi(id, Direccion, X);
			
			char nextItem = mVillPos[zombi[id].getI()][zombi[id].getJ()].get();
			if (nextItem == '*') {
				zombi[id].setCords(ini.getI(), ini.getJ());
			} else if (nextItem == 'Z') {
				zombi[id].setCords(ini.getI(), ini.getJ());
			} else if (nextItem == 'H') {
				zombi[id].setCords(ini.getI(), ini.getJ());
				SimulationManager.getIndicadores().setInd(-20, 0, 0);
			} else {
				mVillPos[ini.getI()][ini.getJ()].set(zombi[id].getHold());
				if (nextItem == '�')
					nextItem = '.';
				zombi[id].setHold(nextItem);
			}
			mVillPos[zombi[id].getI()][zombi[id].getJ()].set('Z');
			SimulationManager.zombiSetCord(id, zombi[id].getI(),
					zombi[id].getJ());
			printPueblo(ini, zombi[id]);
		}
	}

	public char itemAt(int i, int j) {
		return mVillPos[i][j].get();
	}

	public synchronized void printPueblo(Coords ini, Coords end) {
		out.updateScreen(ini, end);
	}

	public void setDataValues() {
		int i, j, idZ = 0;
		for (i = 0; i < this.getX(); i++) {

			for (j = 0; j < this.getX(); j++) {

				if (mVillPos[i][j].get() == 'H') {
					SimulationManager.humanSetCord(i, j);
					human.setCords(i, j);
				} else if (mVillPos[i][j].get() == 'Z') {
					SimulationManager.zombiSetCord(idZ, i, j);
					zombi[idZ] = new Coords(i, j, '.');
					idZ++;
				} else if (mVillPos[i][j].get() != '*'
						&& mVillPos[i][j].get() != '.') {
					nItems++;
				}
			}
		}
	}

	public void setDisplay() {
		Runnable drun = new Runnable() {
			OutputScreen disp;

			@Override
			public void run() {
				try {
					disp = new OutputScreen();
					disp.getFrmZombies().setVisible(true);
					setOut(disp);
				} catch (Exception e) {
				}
			}
		};
		drun.run();
	}

	private void setmVill(char mVill[][]) {
		this.mVill = mVill;
	}

	private void setmVillPos() {
		this.mVillPos = new VillagePos[X][X];
		int i, j;
		for (i = 0; i < this.getX(); i++) {
			for (j = 0; j < this.getX(); j++) {
				mVillPos[i][j] = new VillagePos(mVill[i][j], i, j);
			}
		}
	}

	private void setOut(OutputScreen out) {
		this.out = out;
	}
}
