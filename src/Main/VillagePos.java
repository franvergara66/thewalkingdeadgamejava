package Main;

public class VillagePos {
	private char myItem;
	private int i;
	private int j;

	public VillagePos(char item, int i, int j) {
		this.i = i;
		this.j = j;
		this.myItem = item;
	}

	public char get() {
		return myItem;
	}

	public synchronized char getAndCold() {
		if (myItem == 'Z')
			SimulationManager.dormirZombiThread(i, j,
					SimulationManager.getColdWeaponTime());
		return myItem;
	}

	public synchronized char getAndKill() {
		if (myItem == 'Z') {
			SimulationManager.interruptZombieThread(i, j);
		}
		return myItem;
	}

	public char getAttack(boolean coldAttack) {
		if (coldAttack)
			return getAndCold();
		else
			return getAndKill();
	}

	public synchronized void set(char id) {
		myItem = id;
	}

}
