package Main;

import java.io.*;
import java.util.StringTokenizer;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		char mVill[][];
		int X, i, numZombies, speedZombies, coldWeaponTime, bulletSpeed;
		String line = null;
		StringTokenizer tk;

		FileInputStream fstream = null;
		try {
			fstream = new FileInputStream("C:/ProyectoJava2012/proyecto3.in");
		} catch (FileNotFoundException e) {
			System.exit(-1);
		}
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));

		try {
			if ((line = br.readLine()) == null) {
				System.out.print("Error archivo vacio...");
				System.exit(0);
			}
		} catch (IOException e) {
		}

		tk = new StringTokenizer(line);

		X = Integer.parseInt(tk.nextToken());
		numZombies = Integer.parseInt(tk.nextToken());
		speedZombies = Integer.parseInt(tk.nextToken());
		coldWeaponTime = Integer.parseInt(tk.nextToken());
		bulletSpeed = Integer.parseInt(tk.nextToken());

		System.out.println(X + " " + numZombies + " " + speedZombies + " "
				+ coldWeaponTime + " " + bulletSpeed);

		mVill = new char[X][X];
		for (i = 0; i < X; i++) {
			try {
				mVill[i] = br.readLine().toCharArray();
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
		}
		new SimulationManager(mVill, X, numZombies, speedZombies,
				coldWeaponTime, bulletSpeed);
	}
}
