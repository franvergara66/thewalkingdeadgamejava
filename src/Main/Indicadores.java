package Main;

public class Indicadores {
	private int vida = 100;
	private int puntuacion = 0;
	private int municion = 0;
	private int totalMun = 0;

	public synchronized int getMunicion() {
		return municion;
	}

	public synchronized int getPuntuacion() {
		return puntuacion;
	}

	public synchronized int getTotalMun() {
		return totalMun;
	}

	public synchronized int getVida() {
		return vida;
	}

	public synchronized void setInd(int vida, int puntuacion, int municion) {
		this.vida = this.vida + vida;
		if (this.vida > 100)
			this.vida = 100;
		this.puntuacion = this.puntuacion + puntuacion;
		this.municion = this.municion + municion;
		this.totalMun = this.totalMun + municion;
		if (this.municion < 0)
			this.municion = 0;
		if (this.totalMun < -1)
			this.totalMun = -1;

		SimulationManager.getOut().getMunicionText()
				.setText(String.valueOf(this.municion));
		SimulationManager.getOut().getProgressBar().setValue(this.vida);
		SimulationManager.getOut().getPuntuacionText()
				.setText(String.valueOf(this.puntuacion));
	}
}
